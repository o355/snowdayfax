# Snow Day Fax
# (c) 2020  o355
# This code is licensed under the MIT License. See LICENSE for more information.

from twilio.rest import Client
import pysftp
from selenium import webdriver
from selenium.webdriver.firefox.options import Options
from PIL import Image
firefox_options = Options()
firefox_options.headless = True

# Configuration variables start here

# The path to a geckodriver file for Snow Day Fax to use.
geckodriver_path = ""

# The URL of the webpage that the Snow Day Fax should capture.
capture_website = ""

# The path of the initial .png capture for the Snow Day Fax. You can just put a filename (such as snowdaycapture.png)
# for the capture to be put in the local directory.
capture_filename = ""

# The size, in pixels, of the capture. 1080x1398 best corresponds with an 8.5x11 A4 letter paper, but you can change
# this as you wish.
capture_width = 1080
capture_height = 1398

# The path of the converted .pdf capture for the Snow Day Fax. You can just put a filename (such as snowdaycapture.pdf)
# for the capture to be put in the local directory.
pdf_capture_filename = ""

# The hostname of the SFTP server to upload the capture to.
sftp_host = ""

# The port of the SFTP server to connect to.
sftp_port = 22

# The username on the SFTP server to log in as.
sftp_username = ""

# The password of the SFTP user from line 32 to log in as.
sftp_password = ""

# The directory that the capture should be put in. Make sure that this can be accessible by the Twilio Media URL
# defined later in the configuration.
sftp_directory = ""

# The Twilio SID of your Twilio account. This can be found on the account dashboard page.
twilio_sid = ""

# The Twilio Auth Token of your Twilio account. This can be found on the account dashboard page.
twilio_authtoken = ""

# The number to send the fax from. This needs to be a Twilio account that has fax capabilities.
# It should be formatted with the country code and no extra characters (such as +18005551234)
twilio_fromnumber = ""

# The URL of the Snow Day Capture on your web server for Twilio to use to fax off to the number you
# enter.
twilio_mediaurl = ""

driver = webdriver.Firefox(executable_path=geckodriver_path, options=firefox_options)


print("Capturing website...")
driver.get(capture_website)
driver.set_window_size(capture_width, capture_height)
driver.save_screenshot(capture_filename)

print("Converting image...")
image = Image.open(capture_filename)
imagepdf = image.convert('RGB')
imagepdf.save(pdf_capture_filename)

print("Uploading capture...")
# You will probably run into a host key error at this line. You can disable host key verification, but you will be
# vulnerable to MiTM attacks if you do so.
srv = pysftp.Connection(host=sftp_host, port=sftp_port, username=sftp_username, password=sftp_password)
with srv.cd(sftp_directory):
    srv.put(pdf_capture_filename)

client = Client(twilio_sid, twilio_authtoken)

print("Number to fax to (please enter as +1 fully formatted, e.g. +18005551234)")
number = input("Enter here: ")

fax = client.fax.faxes \
    .create(
         from_=twilio_fromnumber,
         to=number,
         media_url=twilio_mediaurl
     )

print(fax.sid)
print("The fax is on the way!")
