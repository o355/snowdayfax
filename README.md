# Snow Day Fax

Silly little proof-of-concept Python script to fax the snow day predictor page via Twilio fax

# Spaghet-o-meter
Projects made before late 2020 generally include more spaghetti code, and less object-oriented and efficient code. This is a flaw from how I wrote code - usually focusing on development speed rather than future maintainability. Each project I have now gets a rating from 1 to 10 about the spaghettiness of the code.

Snow Day Fax spaghet-o-meter: **4/10**

The Snow Day Fax generally is spaghetti code, although this was a proof-of-concept. Modularity is achieved with configuration variables, although this should have been outside of fax.py. The script is well organized, although image processing & file uploading could have been achieved in separate object-oriented files.

As the script is a proof-of-concept, I didn't focus on object-oriented programming. The length of the fax.py script (98 lines) contributes to the lower spaghet-o-meter rating.

# Getting started
You will need:
* A computer
* A Python 3 installation
* The following Python 3 libraries: Twilio, Pillow, Selenium, PySFTP (`pip3 install twilio pillow selenium pysftp` to install in your command line)
* A web server that has SFTP capabilities to store the page capture
* A Twilio account with a number that has fax capabilities
* Firefox installed on your computer
* A copy of Geckodriver from https://github.com/mozilla/geckodriver/releases

At the very start of main.py, you'll need to modify some variables to get Snow Day Fax to work correctly. This includes Twilio account credentials, the webpage to capture, among other things.

You'll need to make sure geckodriver is able to work properly on your system. This includes downloading Firefox, and downloading the applicable geckodriver for your system.

Once all the configuration variables are good to go, you'll be able to run the script and input a number to send off the fax to. Make sure this number is formatted with country code and no extra characters (+18005551234 for instance).

There are many improvements that can be made to the Snow Day Fax, but after all this is a proof of concept script.

# License
The Snow Day Fax is licensed under the MIT License.
